import React, { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import axios from "axios";


function AddProduct() {
  const [products, setProducts] = useState([])
  const [allFieldsFilled, setAllFieldsFilled] = useState(false)
  const userLogged = JSON.parse(localStorage.getItem('user'))
  const navigate = useNavigate()

  useEffect(() => {
    !userLogged&&navigate('/login')
    axios.get("http://localhost:4000/api/menu")
      .then(response => setProducts(response.data))
  }, [])

  const titleRef = useRef('')
  const imageRef = useRef('')
  const priceRef = useRef(0)

  const handleInputChange = () => {
    // Check if all fields have a value
    if (titleRef.current.value && imageRef.current.value && priceRef.current.value) {
      setAllFieldsFilled(true)
    } else {
      setAllFieldsFilled(false)
    }
  }

  const ajouter = () =>{
    const newProduct = {
      title: titleRef.current.value,
      image: imageRef.current.value,
      price: parseFloat(priceRef.current.value),
    }
    axios.post("http://localhost:4000/api/menu", newProduct)
      .then(response => {
        setProducts([...products, response.data])
        // Clear the input fields after successfully adding the product
        titleRef.current.value = ''
        imageRef.current.value = ''
        priceRef.current.value = 0
        // Reset the form state
        setAllFieldsFilled(false)
        // Display success message
        alert("Product added successfully!")
      })
  }

  return (
    <div className="bg-gray-900 min-h-screen text-gray-100">
    <div className="container mx-auto py-10">
      <h1 className="text-3xl font-bold text-center mb-10">Add a Product</h1>
      <div className="flex flex-col items-center space-y-4">
        <input ref={titleRef} type="text" placeholder="Title" className="border rounded-lg py-2 px-3 bg-gray-800 text-gray-100 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" onChange={handleInputChange} />
        <input ref={imageRef} type="text" placeholder="Image URL" className="border rounded-lg py-2 px-3 bg-gray-800 text-gray-100 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" onChange={handleInputChange} />
        <input ref={priceRef} type="number" step="0.01" placeholder="Price" className="border rounded-lg py-2 px-3 bg-gray-800 text-gray-100 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" onChange={handleInputChange} />
        <button onClick={ajouter} disabled={!allFieldsFilled} className={`bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg ${allFieldsFilled ? '' : 'opacity-50 cursor-not-allowed'}`}>Ajouter</button>
      </div>
    </div>
  </div>
  )
}

export default AddProduct
