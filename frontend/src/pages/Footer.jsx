import React from 'react';
import { Link } from 'react-router-dom';
const Footer = () => {
  return (
    <footer className="bg-gray-800 text-white">
      <div className="container mx-auto py-6">
        <ul className="flex justify-center space-x-8">
        <Link to="/">  <li><a href="#" className="hover:text-gray-500">Home</a></li></Link>
        <Link to="/About">  <li><a href="#" className="hover:text-gray-500">About Us</a></li></Link>
        <Link to="/Contact">  <li><a href="#" className="hover:text-gray-500">Contact Us</a></li></Link>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;
