import React from "react";


const About = () => {
return (
<div className="bg-gray-800 min-h-screen">
<div className="container mx-auto py-10 px-4 sm:px-6 lg:px-8">
<h1 className="text-5xl text-center font-bold text-white mb-8">About Our Restaurant</h1>
<div className="flex flex-wrap -mx-4 mb-8 items-center">
<div className="md:w-1/3 px-4 mb-4 md:mb-0">
<img
src="/res1.jpg"
alt="Restaurant Image"
className="w-full h-full object-cover"
/>
</div>
<div className="md:w-2/3 px-4">
<p className="text-gray-300 text-lg mb-1">

</p>
<p className="text-gray-300 text-lg mb-4">

</p>
<p className="text-gray-300 text-lg mb-4">
running a successful restaurant requires attention to detail and a focus on providing a high-quality dining experience. By prioritizing aspects such as menu design, food and drink quality, service, and atmosphere, restaurant owners can create a memorable and rewarding experience for their customers.
</p>
</div>
</div>

  </div>
</div>
);
};

export default About;