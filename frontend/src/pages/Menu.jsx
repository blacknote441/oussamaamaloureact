import { useDispatch } from "react-redux"
import { addProduct } from "../redux/actions"
import { useEffect, useState } from "react"
import axios from "axios"
import Cart from "../components/cart"




const Products = () => {
  // State to hold the products data
  const [products, setProducts] = useState({})
  // State to keep track of added products
  const [addedProducts, setAddedProducts] = useState([])
  // State to control the visibility of the cart
  const [isCartOpen, setIsCartOpen] = useState(false)
  
  const dispatch = useDispatch()

  // Fetch the products data from the API on mount
  // json-server --watch data/data.json --port 4500
  useEffect(() => {
    axios.get("http://localhost:4000/api/menu")
      .then(response => setProducts(response.data))
  }, [])

  // Handle adding a product to the cart
  const handleAddProduct = (id, title, price, image) => {
    dispatch(addProduct(id, title, price, image))
    setAddedProducts([...addedProducts, id])
  }

  // Handle opening the cart
  const handleCartOpen = () => {
    setIsCartOpen(true)
  }

  // Handle closing the cart
  const handleCartClose = () => {
    setIsCartOpen(false)
  }

  return (
    <div className="flex justify-center py-10 bg-gray-900 text-white">

 

    {/* Product list */}
    <div className="max-w-screen-lg w-full">
      <h1 className=" text-5xl text-center font-bold mb-5">Menu</h1>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-5">
        {products[0] &&
          products.map((p, i) => (
            <div key={i} className="max-w-sm bg-gray-800 border border-gray-700 rounded-lg shadow-lg carte">
              <img src={p.image} alt={p.title} className="w-full rounded-t-lg object-cover" style={{ height: "15rem" }} />
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight">{p.title}</h5>
                </a>
                <p className="mb-3 font-normal text-gray-400">{p.price}DH</p>
                {!addedProducts.includes(p._id) ? (
                  <button
                    onClick={() => handleAddProduct(p._id, p.title, p.price, p.image)}
                    className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-600 rounded-lg shadow-md hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300"
                  >
                    Add to cart
                    <svg
                      aria-hidden="true"
                      className="w-4 h-4 ml-2 -mr-1"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      ></path>
                    </svg>
                  </button>
                ) : (
                  <button className="inline-flex items-center px-3 py-2 text-sm font-medium text-center bg-gray-400 rounded-lg shadow-md cursor-not-allowed focus:outline-none">
                    Added to cart
                  </button>
                )}
              </div>
            </div>
          ))}
      </div>
    </div>
  
    {/* Cart */}




    
    <div className={`fixed inset-y-0 right-0 w-1/2 z-50 ${isCartOpen ? 'visible' : 'hidden'}`}>
      <Cart addedProducts={addedProducts} setAddedProducts={setAddedProducts} isCartOpen={isCartOpen} handleCartClose={handleCartClose} handleAddProduct={handleAddProduct} />
    </div>
  




    {/* Cart toggle button */}
    <button
    onClick={handleCartOpen}
    className="fixed bottom-10 right-10 bg-orange-600 hover:bg-orange-700 text-white font-semibold py-4 inline-flex items-center mr-2 px-4 rounded-full text-xl focus:outline-none shadow-lg"
  >
  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-egg-fried inline-block mr-2" viewBox="0 0 16 16">
    <path d="M10.117 5.653c-.71-.715-1.678-1.153-2.736-1.153s-2.026.438-2.736 1.153C4.053 6.362 3 7.574 3 9v1h10V9c0-1.426-1.053-2.638-2.883-3.347zm-2.342 6.931a2.5 2.5 0 0 1-1.759-.72 2.5 2.5 0 0 1-1.76.72L2.5 12.64V14h11v-1.36l-2.725-.776z"/>
  </svg>
My food
</button>
        </div>
  )
}

export default Products;
