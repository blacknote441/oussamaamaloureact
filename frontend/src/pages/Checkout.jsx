const Checkout = () => {
  return (

   
    <div className="min-h-screen py-6 flex flex-col justify-center sm:py-12 bg-gray-900">
    <div className="relative py-3 sm:max-w-xl mx-auto text-center">
      <span className="text-2xl font-light text-white">Checkout</span>
      <div className="relative mt-4 bg-gray-800 shadow-md sm:rounded-lg text-left">
        <div className="h-2 bg-indigo-400 rounded-t-md"></div>
        <form className="px-8 py-6">
          <div className="grid grid-cols-1 gap-6">
            <label className="block">
              <span className="text-gray-300">Name on Card</span>
              <input type="text" className="form-input mt-1 block w-full rounded-md bg-gray-700 px-4 py-2" />
            </label>

            <label className="block">
              <span className="text-gray-300">Card Number</span>
              <input type="text" className="form-input mt-1 block w-full rounded-md bg-gray-700 px-4 py-2" />
            </label>

            <label className="block">
              <span className="text-gray-300">Expiration Date</span>
              <input type="text" className="form-input mt-1 block w-full rounded-md bg-gray-700 px-4 py-2" />
            </label>

            <label className="block">
              <span className="text-gray-300">CVV</span>
              <input type="text" className="form-input mt-1 block w-full rounded-md bg-gray-700 px-4 py-2" />
            </label>
          </div>

          <div className="mt-6">
            <button type="submit" className="py-3 bg-indigo-600 text-white w-full rounded-md hover:bg-indigo-500">
              Pay Now
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  );
};

export default Checkout;