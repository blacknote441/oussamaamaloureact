import React from "react";

function Contact() {
  return (
    <div className="min-h-screen bg-gray-800 text-white py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 sm:max-w-xl sm:mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-teal-400 to-blue-500 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
        <div className="relative px-4 py-10 bg-gray-900 shadow-lg sm:rounded-3xl sm:p-20">
          <h1 className="text-3xl font-bold mb-5">Contact Us</h1>
          <form>
            <div className="mb-4">
              <label htmlFor="email" className="block text-gray-300 font-bold mb-2">
                Email address
              </label>
              <input
                type="email"
                id="email"
                name="email"
                placeholder="Enter email"
                autoComplete="email"
                className="border-gray-600 rounded-md py-3 px-4 w-full focus:outline-none focus:border-blue-500"
              />
            </div>
            <div className="mb-4">
              <label htmlFor="subject" className="block text-gray-300 font-bold mb-2">
                Subject
              </label>
              <input
                type="text"
                id="subject"
                name="subject"
                placeholder="Enter subject"
                autoComplete="off"
                className="border-gray-600 rounded-md py-3 px-4 w-full focus:outline-none focus:border-blue-500"
              />
            </div>
            <div className="mb-4">
              <label htmlFor="message" className="block text-gray-300 font-bold mb-2">
                Message
              </label>
              <textarea
                id="message"
                name="message"
                rows="5"
                placeholder="Enter message"
                className="border-gray-600 rounded-md py-3 px-4 w-full resize-none focus:outline-none focus:border-blue-500"
              ></textarea>
            </div>
            <button className="bg-blue-500 text-white font-bold py-2 px-4 rounded focus:outline-none hover:bg-blue-700">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Contact;
