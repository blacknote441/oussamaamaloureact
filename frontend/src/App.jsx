import { Routes, Route } from "react-router-dom"
import Home from "./pages/Home"
import Products from "./pages/Menu"
import About from "./pages/About"
import Contact from "./pages/contact"
import Navbar from "./components/navbar"
import Footer from "./pages/Footer"
import Checkout from "./pages/Checkout"
import Login from "./pages/Login"
import Signup from "./pages/Signup"


import AddProduct from "./pages/AddProduct"

function App() {

  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Menu" element={<Products />} />
        <Route path="/About" element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/Checkout" element={<Checkout />} />
        <Route path="/admin" element={<AddProduct />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
      </Routes>
      <Footer />
    </>
  )
}

export default App
