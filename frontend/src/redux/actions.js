import * as type from './typeActions'

export const addProduct = (_id, title, price,image, quantity=1) => {
    return {type:type.ADD_PRODUCT, payload: {_id, title, price, image, quantity}}
}

export const removeProduct = (_id) => {
    return {type:type.REMOVE_PRODUCT, payload: _id}
}

export const emptyCart = () => {
    return {type:type.EMPTY_CART}
}

export const incrementQnt = (_id) => {
    return {type:type.INCREMENT_QNT, payload: _id}
}

export const decrementQnt = (_id) => {
    return {type:type.DECREMENT_QNT, payload: _id}
}