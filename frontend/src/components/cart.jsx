import { useSelector, useDispatch } from "react-redux"
import { removeProduct, incrementQnt, decrementQnt, emptyCart } from "../redux/actions"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"

const Cart = ({ addedProducts, setAddedProducts, isCartOpen, handleCartClose }) => {

    const cart = useSelector(state => state.cart)
    const [total, setTotal] = useState(0)
    const dispatch = useDispatch()

    useEffect(() => {
        setTotal(
            cart.map((product) => product.price * product.quantity).reduce((total, price) => total + parseFloat(price), 0)
        )
    }, [cart])

    const handleIncrement = (product) => {
        dispatch(incrementQnt(product._id))
        setTotal(prev => prev + parseFloat(product.price))
    }

    const handleDecrement = (product) => {
        dispatch(decrementQnt(product._id))
        setTotal(prev => prev - parseFloat(product.price))
    }

    const handleRemoveProduct = (id, price) => {
        dispatch(removeProduct(id))
        setAddedProducts(addedProducts.filter(productId => productId !== id))
        setTotal(prev => prev - parseFloat(price))
    }

    const handleEmptyCart = () => {
        dispatch(emptyCart())
        setAddedProducts([])
    }

    return (
        <div className="fixed inset-0 overflow-hidden">
            <div className="absolute inset-0 bg-gray-900 opacity-75"></div>
            <div className="fixed inset-y-0 right-0 max-w-full flex">
                <div className="w-screen max-w-md">
                    <div className="h-full flex flex-col bg-gray-800 shadow-xl">
                        <div className="flex justify-end pt-4 pr-4">
                            <button onClick={handleCartClose} className="text-white focus:outline-none">&times;</button>

                        </div>
                        <button onClick={handleEmptyCart} className="flex items-center justify-center bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded-md focus:outline-none">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
                          <path fillRule="evenodd" d="M17.707 3.293a1 1 0 00-1.414 0L10 9.586 4.707 4.293a1 1 0 00-1.414 1.414L8.586 11l-5.293 5.293a1 1 0 001.414 1.414L10 12.414l5.293 5.293a1 1 0 001.414-1.414L11.414 11l5.293-5.293a1 1 0 000-1.414z" clipRule="evenodd" />
                        </svg>
                        Empty Cart
                      </button>
                        <h1 className="text-3xl font-bold text-white px-5 mb-6">My Food</h1>
                        {cart.length === 0 && <p className="px-5 text-white">Your cart is currently empty!</p>}
                        {cart.length > 0 && (
                            <>
                            <div className="divide-y divide-gray-700 overflow-y-scroll flex-grow">
                                {cart.map((product, index) => (
                                    <div key={index} className="flex py-6 px-5 items-center justify-between">
                                        <div className="flex items-center">
                                            <div className="h-12 w-12 mr-4 overflow-hidden">
                                                <img src={product.image} alt={product.title} className="h-full transform hover:scale-110 transition duration-200 ease-in-out" />
                                            </div>
                                            
                                            <div>
                                                <h2 className="text-lg font-bold text-white">{product.title}</h2>
                                                <p className="text-gray-400">{product.description}</p>
                                                <p className="text-gray-500">{product.price.toFixed(2)}DH x {product.quantity}</p>
                                            </div>
                                        </div>
                                        <div className="flex flex-col md:flex-row items-center justify-between">
                                        
                                            <div className="flex items-center mb-2 md:mb-0">
                                                <button onClick={() => handleRemoveProduct(product._id, product.price)} className="bg-red-500 text-white py-1 px-2 rounded-md mr-3 hover:bg-red-600 focus:outline-none">
                                                    Remove
                                                </button>
                                            </div>
                                            <div className="flex items-center ml-2">
                                                <button onClick={() => handleDecrement(product)} className="bg-gray-700 text-white py-1 px-2 rounded-l-md hover:bg-gray-600 focus:outline-none">-</button>
                                                <span className="mx-2 text-white">{product.quantity}</span>
                                                <button onClick={() => handleIncrement(product)} className="bg-gray-700 text-white py-1 px-2 rounded-r-md hover:bg-gray-600 focus:outline-none">+</button>
                                            </div>
                                        </div>

                                    </div>
                                ))}
                            </div>
                            <div className="border-t border-gray-700 px-5 py-4">
                                <div className="flex justify-between text-lg font-bold text-white mb-3">
                                    <p>total</p>
                                    <p>{total.toFixed(2)}DH</p>
                                </div>
                                <div className="mt-6">
                                <Link to="/Checkout" onClick={handleCartClose} className="flex items-center justify-center bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md mr-3 focus:outline-none">
                                  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M2.5 6.5A1.5 1.5 0 014 5h12a1.5 1.5 0 011.5 1.5v8a1.5 1.5 0 01-1.5 1.5H4a1.5 1.5 0 01-1.5-1.5v-8zm0-2C1.119 4.5.5 5.119.5 6v8c0 .881.619 1.5 1.5 1.5h12c.881 0 1.5-.619 1.5-1.5v-8c0-.881-.619-1.5-1.5-1.5h-12z" clipRule="evenodd"/>
                                    <path fillRule="evenodd" d="M3 4.5A2.5 2.5 0 015.5 2h9A2.5 2.5 0 0117 4.5v8a2.5 2.5 0 01-2.5 2.5h-9A2.5 2.5 0 013 12.5v-8zm2-1.5a1.5 1.5 0 00-1.5 1.5v8c0 .827.673 1.5 1.5 1.5h9a1.5 1.5 0 001.5-1.5v-8a1.5 1.5 0 00-1.5-1.5h-9z" clipRule="evenodd"/>
                                    <path fillRule="evenodd" d="M6.5 9.5A.5.5 0 017 9h6a.5.5 0 010 1H7a.5.5 0 01-.5-.5zM6.5 11.5A.5.5 0 017 11h3a.5.5 0 010 1H7a.5.5 0 01-.5-.5zM10.5 13.5A.5.5 0 0111 13h1a.5.5 0 010 1h-1a.5.5 0 01-.5-.5zM6.5 13.5A.5.5 0 017 13h1a.5.5 0 010 1H7a.5.5 0 01-.5-.5z" clipRule="evenodd"/>
                                  </svg>
                                  Checkout
                                </Link>
                              </div>
                                    </div>
                                    </>
                                    )}
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    )
                                    }
                                    
                                    export default Cart