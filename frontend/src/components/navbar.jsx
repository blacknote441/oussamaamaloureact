import { useState } from 'react';
import { Link } from 'react-router-dom';
import useLogout from '../hooks/useLogout';

const Navbar = () => {
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const userLogged = JSON.parse(localStorage.getItem('user'))
  const {logout} = useLogout()

  const handleMenuButtonClick = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
  };

  const handleLogout = () => {
    logout()
    window.location.reload(false);
  }

  return (
    <nav className="bg-gray-800">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex items-center justify-between h-16">
          <Link to="/" className="flex-shrink-0">
            <span className="text-xl font-bold text-white">Restaurant OS</span>
          </Link>

          <div className="-mr-2 flex md:hidden">
            <button
              type="button"
              onClick={handleMenuButtonClick}
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:bg-gray-700 focus:text-white transition duration-150 ease-in-out"
              aria-label="Main menu"
              aria-expanded={isMobileMenuOpen}
            >
              <svg className={`${isMobileMenuOpen ? 'hidden' : 'block'} h-6 w-6`} stroke="currentColor" fill="none" viewBox="0 0 24 24">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
              <svg className={`${isMobileMenuOpen ? 'block' : 'hidden'} h-6 w-6`} stroke="currentColor" fill="none" viewBox="0 0 24 24">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12"></path>
              </svg>
            </button>
          </div>

          <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
            <Link to="/" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Home</Link>
            <Link to="/menu" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Menu</Link>
            <Link to="/about" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">About</Link>
            <Link to="/contact" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Contact</Link>
            {!userLogged && <Link to="/signup" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Signup</Link>}
            {!userLogged && <Link to="/login" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Login</Link>}
            {userLogged && <Link to="/admin" className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Admin</Link>}
            {userLogged && <Link onClick={handleLogout} className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Logout</Link>}
          </div>
        </div>
      </div>

      {isMobileMenuOpen && (
        <div className="md:hidden">
          <div className="px-2 pt-2 pb-3 sm:px-3">
            <Link to="/" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Home</Link>
            <Link to="/menu" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Menu</Link>
            <Link to="/about" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">About</Link>
            <Link to="/contact" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Contact</Link>
            {!userLogged && <Link to="/signup" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Signup</Link>}
            {!userLogged && <Link to="/login" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Login</Link>}
            {userLogged && <Link to="/admin" className="block text-white font-medium py-2 px-3 rounded-md hover:bg-gray-700">Admin</Link>}
            {userLogged && <Link onClick={handleLogout} className="text-white hover:bg-gray-700 rounded-md py-2 px-3 text-sm font-medium">Logout</Link>}
          </div>
        </div>
      )}
    </nav>
  );
};

export default Navbar;
