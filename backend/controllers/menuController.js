import Menu from "../models/menuModel.js";
import { Types } from "mongoose";

// GET all products
export const getProducts = async (_, res) => {
  try {
    const products = await Menu.find({}).sort({ createdAt: -1 });
    res.status(200).json(products);
  } catch (error) {
    res.status(500).send(error);
  }
};


// CREATE a new product
export const createProduct = async (req, res) => {
  const { title, price, image } = req.body;

  try {
    const product = await Menu.create({
      title,
      price,
      image,
    });
    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// DELETE a product
export const deleteProduct = async (req, res) => {
  const { id } = req.params;

  if (!Types.ObjectId.isValid(id)) {
    return res.status(404).json({ error: "Product not found" });
  }

  const product = await Menu.findOneAndDelete({ _id: id });

  if (!product) {
    return res.status(400).json({ error: "Product not found" });
  }

  res.status(200).json(product);
};
