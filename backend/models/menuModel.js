import { Schema, model } from "mongoose";

const menuSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Menu = model("Menu", menuSchema);
export default Menu;
