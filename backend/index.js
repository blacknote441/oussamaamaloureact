import express, { json } from "express";
import mongoose from "mongoose";
import cors from "cors";

//import routes
import menuRoutes from "./routes/menu.js";
import userRoutes from "./routes/user.js";

// to access .env variables
import dotenv from "dotenv";
dotenv.config();

const app = express();
const port = process.env.PORT;

// middleware
app.use(json());
app.use(cors());

// connect to the mongodb cluster
mongoose
  .connect(process.env.DB_URL)
  .then(() => {
    app.listen(port, () => {
      console.log("connected to db & listening on port", port);
    });
  })
  .catch((error) => {
    console.log(error);
  });

// routes
app.use("/api/menu", menuRoutes);
app.use("/api/user", userRoutes);
