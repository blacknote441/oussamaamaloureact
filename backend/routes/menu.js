import { Router } from "express";
import { createProduct, deleteProduct, getProducts } from "../controllers/menuController.js";

const router = Router()

// GET all products
router.get("/", getProducts)


// CREATE a new product
router.post("/", createProduct)

// DELETE a product
router.delete("/:id", deleteProduct)

export default router
